import React from 'react';
// import './App.css';
import {BrowserRouter as Router, Route,Switch} from 'react-router-dom'
import NavBar from './Component/NavBar'
import About from './Component/About'
import Home from './Component/Home'
import Login from './Component/Login'
import Contact from './Component/Contact'
import Log from './Component/AddLogin/Log'

function App() {
  return (
    <Router>
      <div className="App">
            <NavBar />
            <Switch>
            <Route exact path='/' component ={Home}/>
            <Route path='/about' component={About} />  
            <Route path='/login' component={Login} />
            <Route path='/log' component={Log} />
            <Route path='/contact' component={Contact} />
            </Switch>
      </div>
    </Router>
  );
}

export default App;
