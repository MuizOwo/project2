import React from 'react'
import '../App.css'

function About() {
           return (
                     <div className='UU'>
                          <div className="row dark text-center">
                              <h3 className="heading">Built With Care</h3>
                          </div>

                      <div className='Q'>
                      <div className="col-md-4">
                          <h3>React.JS</h3>
                          <div className="features">
                              <i className="fab fa-react fa-3x"></i>  
                          </div>
                          <p className="lead">Built with the latest, React.js.</p>
                      </div>

                      <div className="col-md-4">
                          <h3>Bootsrap</h3>
                          <div className="features">
                              <i className="fas fa-bold fa-3x"></i>  
                          </div>
                          <p className="lead">Built with the latest, Bootsrap css .</p>
                      </div>

                      <div className="col-md-4">
                          <h3>CSS 3</h3>
                          <div className="features">
                              <i className="fab fa-css3 fa-3x"></i>  
                          </div>
                          <p className="lead">Built with the latest, CSS 3.</p>
                      </div>
                      </div>
              
           </div>        
              
           )
}

export default About
