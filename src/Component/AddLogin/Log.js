import React from 'react'


const Log=({Name,Password})=> {
           return (
                      <div className="row">
                      <div className="col s12 m6">
                            <div className="card blue-grey darken-1">
                            <div className="card-content white-text">
                            <span className="card-title">{Name}  <i class="far fa-edit"></i></span> 
                            <div className='divider'></div>
                            <p>{Password}</p>
                            </div>
                            </div>
                      </div>
                      </div>
           )
}

export default Log
