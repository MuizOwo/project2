import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import '../App.css'

class NavBar extends Component {
           render() {
                      return (
                                 <nav>
                                 <Link to='/'><h3>MuizOwo</h3></Link>
                                            <ul className="nav-link">
                                                       <Link to='/about'>About us </Link>
                                                       <Link to='/contact'>Contact</Link>
                                                       <Link to='/login'>Log in</Link>
                                            </ul>
                                 </nav>
                      )
           }
}

export default NavBar
