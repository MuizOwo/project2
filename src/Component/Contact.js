import React, { Component } from 'react'
import '../App.css'


class Contact extends Component {
           render() {
                      return (
                                 <footer className="page-footer">
                                 <div className="container">
                                   <div className="row">
                                     <div className="col l6 s12">
                                     <div className='center'>
                                     <i className="fab fa-monero fa-4x"></i>
                                     </div>
                                       <p>At our core is a collection of design and development solutions that represent everytjing your buisness needs to compete in the modern marketplace.</p>
                                      <strong>Contact Info</strong>
                                      <p>018-3867-619 <br />muizowo@gmail.com</p>
                                     </div>
                                     <div className="col l4 offset-l2 s12">
                                       <ul>
                                       <li><a href="https://www.facebook.com/omolade.owodunni" target="_black"><i className="fab fa-facebook-square"></i></a></li>
                                       <li><a href="https://twitter.com/muiz_owo" target="_black"><i className="fab fa-twitter-square"></i></a></li>
                                       <li><a href="https://www.instagram.com/muiz_owo14/" target="_black"><i className="fab fa-instagram"></i></a></li>
                                       </ul>
                                     </div>
                                   </div>
                                 </div>
                                 <div className="footer-copyright">
                                   <div className="container">
                                   &copy; Muiz Theme
                                   </div>
                                 </div>
                               </footer>
                      )
           }
}

export default Contact
